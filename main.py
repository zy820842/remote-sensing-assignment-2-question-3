# -*- coding: utf-8 -*-
"""
Created on Mon Mar 16 09:51:40 2020

@author: zy820842
"""

"""Plot the brightness temperature for different frequencies."""

import radiative_transfer as rad
import parameters as p
import optical_thickness as op
import numpy as np
import matplotlib.pyplot as plt

def main(): 
    f = np.linspace(1, 30) #Define frequency between 1 and 30 GHz. 
    T = np.zeros(len(f)) #Initialise the brightness temperature as a zero 
    #vector that is the same size as f. 
    Tr = np.zeros(len(f)) #Initialise the transmittance as a zero vector that 
    #is the same size as f.
    Ts = np.zeros(len(f)) #Initialise the surface temperature as a zero vector,
    #that is the same size as f.
    Ta = np.zeros(len(f)) #Initialise the average temperature as a zero vector
    #that is the same size as f.
    
    for i in range(len(f)):
        T[i] = rad.radiative(f[i])
        Tr[i] = np.exp(-op.opthick(f[i]))
        Ts[i] = p.Ts
        Ta[i] = p.Ta
     
        
    plt.figure(1)
#    plt.plot(f, T, label = "Brightness Temperature")
    plt.plot(f, Tr, ':', label = "Transmittance")
#    plt.plot(f, Ts, '--', label = "Ocean Brightness Temperature")
#    plt.plot(f, Ta, 'o', label = "Average Temperature")
    plt.xlabel('frequency (Ghz)')
    plt.ylabel('Transmittance (Wm^{-2}sr^{-1}mu m^{-1})')
    plt.legend()
    plt.savefig('Transmittance.jpg')
    plt.show
    
if __name__=="__main__":
    main()