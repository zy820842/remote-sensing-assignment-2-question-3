# -*- coding: utf-8 -*-
"""
Created on Mon Mar 16 09:37:40 2020

@author: zy820842
"""

"""Parameters for calculating the radiative transfer equation in terms of 
brightness temperature."""

w = 1.5 #Water content in gm^{-3}
Ta = 212.5 #Average temperature in atmosphere, in K
Ts = 150 #Ocean brightness temperature, in K
R = 0.5 #Ocean reflectivity 
L = 2 #Cloud thickness, in km.

def absorption(f):
    """Defines the volume absorption coefficient of water in km^{-1}. f is the
    frequency given in GHz and w is in gm^{-3}."""
    alpha = 2.4*10**(-4)*f**(1.95)*w
    
    return alpha