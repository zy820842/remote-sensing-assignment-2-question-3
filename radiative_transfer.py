# -*- coding: utf-8 -*-
"""
Created on Mon Mar 16 09:47:13 2020

@author: zy820842
"""

"""Using a simplified temperature profile, we can use a simplified radiative 
transfer equation to calculate the brightness temperature."""

import parameters as p
import optical_thickness as op
import numpy as np

def radiative(f):
    """Calculate the brightness temperature using the simplified radiative 
    transfer equation."""
    Tb = p.R*p.Ts*np.exp(-op.opthick(f)) + p.Ta*(1 - np.exp(-op.opthick(f))) \
    + np.exp(-op.opthick(f))*p.Ta*(1 - np.exp(-op.opthick(f)))*(1 - p.R)
    
    return Tb