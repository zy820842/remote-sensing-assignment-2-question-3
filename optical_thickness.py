# -*- coding: utf-8 -*-
"""
Created on Mon Mar 16 09:43:34 2020

@author: zy820842
"""

"""This calculates the optical thickness of the cloud, which is used in the 
radiative transfer equation."""

import parameters as p

def opthick(f):
    """Optical thickness calculated by multiplying the geometric thickness and
    the aborption coefficient."""
    tau = p.L*p.absorption(f)
    
    return tau